/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author s6yuan, shy051
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}
	public String toString() {
		return Float.toString(getValue()) + " C";
	}
	@Override
	public Temperature toCelsius() {
		Temperature temps = new Celsius(getValue());
		return temps;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp;
		temp = getValue();
		temp = ((temp) *9)/5 + 32;
		Temperature temps = new Fahrenheit(temp);
		return temps;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}

/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author s6yuan, shy051
 *
 */
public class Fahrenheit extends Temperature{
	public Fahrenheit(float t) {
		super(t);
	}
	public String toString() {
		return Float.toString(getValue()) + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float temp;
		temp = getValue();
		temp = ((temp - 32)*5)/9;
		Temperature temps = new Celsius(temp);
		return temps;
	}
	@Override
	public Temperature toFahrenheit() {
		Temperature temps = new Fahrenheit(getValue());
		return temps;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}

package edu.ucsd.cs110w.temperature;

/**
 * TODO (s6yuan): write class javadoc
 *
 * @author s6yuan
 */

public class Kelvin extends Temperature {
	public Kelvin(float t) {
		super(t);
	}
	public String toString( ) {
		return Float.toString(getValue()) + " K";
	}
	@Override
	public Temperature toCelsius( ) {
		float temp;
		temp = getValue();
		temp -= 273.15;
		Temperature temps = new Fahrenheit(temp);
		return temps;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp;
		temp = getValue();
		temp = (float) (((temp - 273.15)*5)/9) + 32;
		Temperature temps = new Fahrenheit(temp);
		return temps;
	}
	@Override
	public Temperature toKelvin() {
		Temperature temps = new Kelvin(getValue());
		return temps;
	}
	
}
